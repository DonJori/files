package com.techuniversity.files.controllers;

import com.techuniversity.files.storage.FileSystemStorageService;
import com.techuniversity.files.storage.StorageFileNotFoundException;
import com.techuniversity.files.storage.StorageProperties;
import com.techuniversity.files.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoProperties;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class FileUploadController {
    private FileSystemStorageService storageService () {
        return new FileSystemStorageService(new StorageProperties());
    }

    @GetMapping("/")
    public String listUploadedFiles(Model model) throws IOException {
        Stream<Path> paths = storageService().loadAll();
        Stream<String> links = paths.map(
                path -> {
                    UriComponentsBuilder ucb = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString());
                    return ucb.build().toUri().toString();
                }
        );
        List<String> listLinks = links.collect(Collectors.toList());
        model.addAttribute("files", listLinks);

        return "uploadForm";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        Resource file = storageService().loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @GetMapping(value = "/files/{filename:.+}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getImage(@PathVariable String filename) throws IOException {
        Resource file = storageService().loadAsResource(filename);
        InputStream is = file.getInputStream();
        byte[] image = new byte[is.available()];
        is.read(image);

        return image;
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        storageService().store(file);
        redirectAttributes.addFlashAttribute("message", "GC. Has subido correctamente el archivo: " + file.getOriginalFilename());
        return "redirect:/";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
